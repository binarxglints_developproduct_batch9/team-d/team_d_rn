import React, {useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {loginAction} from '../actions/authAction';
import {userAction} from '../actions/userAction';
import qs from 'qs';

const loginPage = ({navigation}) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();
  const userList = useSelector((state) => state.user);
  const payload = useSelector((state) => state.auth);
  const [nama, setNama] = useState('');
  const handleLogin = () => {
    let user = qs.stringify({
      email,
      password,
    });
    dispatch(loginAction(user)).then((e) => {
      if (e) {
        dispatch(userAction({token: e})).then((el) => {
          console.log('el', el);
          if (el) {
            ToastAndroid.show(`Welcome ${el.fullname}!`, 2000);
          }
        });
      }
    });
  };
  console.log('nama anda', nama);

  return (
    <View style={style.container}>
      <Image
        style={style.logo}
        source={{
          uri:
            'https://cdn.discordapp.com/attachments/795872155531214868/796383487249612841/Drawing_3.png',
        }}
      />
      <Text style={style.tag}>Email</Text>
      <TextInput
        style={style.inputText}
        onChangeText={(email) => setEmail(email)}
        value={email}
      />
      <Text style={style.tag}>Password</Text>
      <TextInput
        style={style.inputText}
        secureTextEntry={true}
        onChangeText={(password) => setPassword(password)}
        value={password}
      />
      {payload.error && (
        <Text style={{color: 'white'}}>Check Your Email or Password</Text>
      )}
      <TouchableOpacity
        style={style.loginBtn}
        onPress={async () => {
          await handleLogin();
        }}>
        <Text style={style.textLogin}>Login</Text>
      </TouchableOpacity>
      <Text style={style.textAccount}>
        Don't have an account?{' '}
        <Text
          style={{fontWeight: 'bold', color: '#28df99'}}
          onPress={() => navigation.navigate('signUpPage')}>
          Sign Up
        </Text>
      </Text>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#313131',
  },
  logo: {
    width: '35%',
    height: null,
    aspectRatio: 2,
    marginBottom: 50,
  },
  tag: {
    color: '#28df99',
    fontWeight: 'bold',
    fontSize: 16,
    marginBottom: 5,
    width: '90%',
  },
  inputText: {
    width: '90%',
    borderRadius: 7,
    padding: 10,
    // backgroundColor: '#545454',
    color: 'white',
    marginBottom: 10,
    borderColor: '#545454',
    borderWidth: 1,
  },
  loginBtn: {
    width: '90%',
    borderRadius: 7,
    alignItems: 'center',
    padding: 10,
    marginTop: 100,
    borderColor: '#28df99',
    borderWidth: 1,
  },
  textAccount: {
    color: 'white',
    marginTop: 15,
    marginBottom: 10,
  },
  textLogin: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  //Google Login
  googleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  googleText: {
    color: 'white',
  },
  google: {
    height: 25,
    width: 25,
  },
});

export default loginPage;
