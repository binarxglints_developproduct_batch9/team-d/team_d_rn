import React, {useState, useEffect} from 'react';
import {View, TextInput, StyleSheet, FlatList, Text} from 'react-native';

import FieldCard from '../components/FieldCard';
import {useDispatch, useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import {searchFieldAction} from '../actions/searchFieldAction';

const searchField = ({navigation}) => {
  const [fieldname, setFieldName] = useState(null);
  const listField = useSelector((state) => state.search);
  const dispatch = useDispatch();
  const searchField = () => {
    dispatch(searchFieldAction(fieldname));
  };

  return (
    <View style={style.container}>
      <View style={style.container2}>
        <TextInput
          style={style.txtSearch}
          autoCapitalize="words"
          onChangeText={(fieldname) => setFieldName(fieldname)}
          value={fieldname}
        />
        <Icon
          name="search"
          size={27}
          color={'#28df99'}
          onPress={() => searchField()}
          style={style.sendIcon}
        />
      </View>
      <View style={style.fieldContainer}>
        {
          <FlatList
            showsVerticalScrollIndicator={false}
            data={listField.listField}
            renderItem={({item}) => {
              return (
                <FieldCard
                  id={item.id}
                  name={item.fieldName}
                  location={item.location}
                  image={item.image}
                  price={item.price}
                  description={item.description}
                  city={item.city}
                  location={item.location}
                  detailField={() =>
                    navigation.navigate('homeDetails', {
                      id: item.id,
                      name: item.fieldName,
                      location: item.location,
                      image: item.image,
                      price: item.price,
                      description: item.description,
                      city: item.city,
                      location: item.location,
                    })
                  }
                />
              );
            }}
            keyExtractor={(item) => item.id}
          />
        }
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    backgroundColor: '#313131',
    alignItems: 'center',
    height: '100%',
  },
  container2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '95%',
    paddingVertical: 10,
    alignItems: 'center',
  },
  txtSearch: {
    width: '90%',
    borderRadius: 7,
    color: 'white',
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: '#545454',
  },
  fieldContainer: {
    width: '100%',
    borderTopWidth: 1,
    borderColor: 'white',
    flex: 1,
  },
});
export default searchField;
