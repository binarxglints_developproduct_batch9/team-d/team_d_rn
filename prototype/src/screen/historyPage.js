import React, { useEffect, useState } from 'react';
import { Text, View, ScrollView, StyleSheet, FlatList, ActivityIndicator } from 'react-native';
import HeaderHistory from '../components/HeaderHistory';
import {getHistoryAction} from '../actions/historyAction';
import {useDispatch, useSelector} from 'react-redux';
import HistoryCard from '../components/HistoryCard';

const historyPage = ({navigation}) => {
  const auth = useSelector((state) => state.auth);
  const [loading, setLoading] = useState(true)
  const historyList = useSelector((state) => state.history);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getHistoryAction({ token: auth.payload }))
    .then(() => setLoading(false))
  }, []);

  if (loading == true) {
    return (
      <View style={{flex: 1,  alignItems: 'center', justifyContent: 'center', backgroundColor: '#313131' }}>
        <ActivityIndicator size="large" color="#28df99" />
      </View>
    )
  }


  return (
    <View style={style.container}>
      <View style={style.header}>
        <Text style={style.tag}>Book History</Text>
      </View>
      <View style={style.scrollContainer}>
        <FlatList
          showsVerticalScrollIndicator={false}
          data={historyList.history}
          renderItem={({item}) => {
            return (
              <HistoryCard
                name={item.field}
                date={item.date}
                id={item.id}
                transaction={item.transaction}
                fullname={item.fullname}
              />
            );
          }}
        />
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#313131',
    alignItems: 'center',
    // paddingBottom: '14%',
  },
  header: {
    width: '100%',
    height: '8%',
    alignItems: 'center',
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderColor: 'white',
  },
  tag: {
    fontWeight: 'bold',
    fontSize: 18,
    color: '#28df99',
  },
  scrollContainer: {
    width: '90%',
    paddingTop: 10,
    flex: 1,
  },
});

export default historyPage;
