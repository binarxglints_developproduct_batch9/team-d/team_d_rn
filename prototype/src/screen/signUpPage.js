import React, { useState, useEffect } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  ToastAndroid
} from 'react-native';
import { Picker } from '@react-native-picker/picker';
import { useDispatch, useSelector } from 'react-redux';
import { registerAction } from '../actions/authAction';
import qs from 'qs';

const signUpPage = ({ navigation }) => {
  const [fullname, setFullname] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordConfirmation, setPasswordConfirmation] = useState("");

  const dispatch = useDispatch();
  const payload = useSelector((state) => state.auth);
  const handleRegister = () => {
    let user = qs.stringify({
      fullname,
      email,
      password,
      passwordConfirmation,
    });
    dispatch(registerAction(user));
  };
  useEffect(() => { }, []);
  {
    console.log(payload);
  }
  return (
    <View style={styles.container}>
      <Image
        style={styles.image}
        source={{
          uri:
            'https://cdn.discordapp.com/attachments/795872155531214868/796383487249612841/Drawing_3.png',
        }}
      />
      <Text style={styles.tag}>Full Name</Text>
      <TextInput
        style={styles.inputText}
        onChangeText={(fullname) => setFullname(fullname)}
        value={fullname}
      />
      <Text style={styles.tag}>Email</Text>
      <TextInput
        style={styles.inputText}
        onChangeText={(email) => setEmail(email)}
        value={email}
      />
      <Text style={styles.tag}>Password</Text>
      <TextInput
        secureTextEntry={true}
        style={styles.inputText}
        onChangeText={(password) => setPassword(password)}
        value={password}
      />
      <Text style={styles.tag}>Confirm Password</Text>
      <TextInput
        secureTextEntry={true}
        style={styles.inputText}
        onChangeText={(passwordConfirmation) =>
          setPasswordConfirmation(passwordConfirmation)
        }
        value={passwordConfirmation}
      />
      <TouchableOpacity
        style={styles.loginBtn}
        onPress={() => {
          
            if (
              fullname === "" || email === "" || password === "" || passwordConfirmation === ""
            ){ToastAndroid.show("You must fill all fields", 2000)}
            else if(
              password.length < 8 || passwordConfirmation.length < 8
            ){ToastAndroid.show("Password must 8 characters", 2000)}
            else if(
              password !== passwordConfirmation
            ){ToastAndroid.show("Check your Password and Password confirmation", 2000)}
            else{
              handleRegister()
            }
            
          
        }}>
        <Text style={styles.textLogin}>Sign Up</Text>
      </TouchableOpacity>
      <Text style={styles.textAccount}>
        Already have an account?{' '}
        <Text
          style={{ fontWeight: 'bold', color: '#28df99' }}
          onPress={() => navigation.navigate('loginPage')}>
          Log In
        </Text>
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#313131',
  },
  image: {
    width: '35%',
    height: null,
    aspectRatio: 2,
    marginBottom: 50,
  },
  tag: {
    color: '#28df99',
    width: '90%',
    fontWeight: 'bold',
    fontSize: 16,
    marginBottom: 5,
  },
  inputText: {
    width: '90%',
    borderRadius: 7,
    // backgroundColor: '#545454',
    color: 'white',
    padding: 10,
    marginBottom: 10,
    borderWidth: 1,
    borderColor: '#545454',
  },
  loginBtn: {
    width: '90%',
    borderRadius: 7,
    // backgroundColor: '#28df99',
    alignItems: 'center',
    padding: 10,
    marginTop: 50,
    borderWidth: 1,
    borderColor: '#28df99',
  },
  textLogin: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  textAccount: {
    marginTop: 10,
    color: '#FFFFFF',
  },
});

export default signUpPage;
