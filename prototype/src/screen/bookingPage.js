import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';

import DatePicker from 'react-native-date-picker';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import {timeSlotAction} from '../actions/timeSlotAction';

const bookingPage = ({route}) => {
  const navigation = useNavigation();
  const [date, setDate] = useState(new Date());
  const auth = useSelector((state) => state.auth);
  const timeSlot = useSelector((state) => state.time);
  let [timeSlotPicked, setTimeSlotPicked] = useState([]);
  const dispatch = useDispatch();
  const getTimeSlot = async () => {
    dispatch(
      timeSlotAction({date, token: auth.payload, id: route.params.id}),
    ).then((e) => {
      setTimeSlotPicked(e);
    });
  };
  useEffect(() => {}, [timeSlotPicked]);

  return (
    <View style={style.container}>
      <View style={style.container2}>
        <Text style={style.modalText}>Booking Date</Text>
        <DatePicker
          date={date}
          onDateChange={(date) => setDate(date)}
          androidVariant={'nativeAndroid'}
          mode={'date'}
          textColor={'white'}
          format="YYYY-MM-DD"
          minimumDate={new Date()}
        />
        <TouchableOpacity
          onPress={() => {
            getTimeSlot();
          }}>
          <Text style={style.dateCheckButton}>Check Availability</Text>
        </TouchableOpacity>
        <View style={style.divider}></View>
        <Text style={style.modalText2}>Booking Time</Text>

        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{height: '25%', width: '90%'}}>
          {timeSlotPicked &&
            timeSlotPicked.map((obj, index) => (
              <TouchableOpacity
                key={index}
                onPress={() => {
                  let newTimeslot = [...timeSlotPicked];
                  if (newTimeslot[index].status) {
                    newTimeslot[index].status = false;
                  } else {
                    newTimeslot[index].status = true;
                  }
                  setTimeSlotPicked(newTimeslot);
                }}>
                <Text
                  style={{
                    color: obj.status ? '#28df99' : '#FFFFFF',
                    textAlign: 'center',
                    marginBottom: 5,
                    borderBottomWidth: 1,
                    borderColor: 'white',
                    fontSize: 16,
                    paddingBottom: 5,
                    marginTop: 5,
                  }}>
                  {obj.time}
                </Text>
              </TouchableOpacity>
            ))}
        </ScrollView>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('confirmationPage', {
              timeSlotPicked,
              date,
              name: route.params.name,
              price: route.params.price,
              id: route.params.id,
            });
            setTimeSlotPicked([]);
          }}>
          <Text style={style.timeCheckButton}>Book</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
const style = StyleSheet.create({
  container: {
    backgroundColor: '#313131',
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  container2: {
    padding: 20,
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#28df99',
    width: '90%',
    borderRadius: 10,
    height: '90%',
  },
  modalText: {
    color: '#FFCB74',
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  modalText2: {
    color: '#FFCB74',
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 10,
    borderTopWidth: 1,
    borderColor: '#28df99',
    width: '90%',
    textAlign: 'center',
    paddingTop: 25,
  },
  dateCheckButton: {
    color: 'white',
    fontSize: 16,
    borderWidth: 1,
    borderRadius: 7,
    borderColor: '#545454',
    padding: 10,
    marginTop: 10,
    marginBottom: 25,
  },
  timeCheckButton: {
    color: 'white',
    fontSize: 16,
    borderWidth: 1,
    borderRadius: 7,
    borderColor: '#545454',
    padding: 10,
    marginTop: 10,
  },
});

export default bookingPage;
