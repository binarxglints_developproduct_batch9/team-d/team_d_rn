import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

import PlayerCard from '../components/PlayerCard';

const playerList = () => (
  <View style={style.container}>
    <View style={style.teamContainer}>
      <TouchableOpacity>
        <Text style={style.tag}>Team A</Text>
      </TouchableOpacity>
      <TouchableOpacity>
        <Text style={style.tag}>Team B</Text>
      </TouchableOpacity>
    </View>
    <View style={style.scrollContainer}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <PlayerCard />
        <PlayerCard />
        <PlayerCard />
        <PlayerCard />
        <PlayerCard />
        <PlayerCard />
        <PlayerCard />
      </ScrollView>
    </View>
  </View>
);

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#313131',
    alignItems: 'center',
  },
  teamContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '90%',
    height: '7%',
    alignItems: 'center',
  },
  tag: {
    color: '#28df99',
    fontWeight: 'bold',
    fontSize: 20,
  },
  scrollContainer: {
    width: '100%',
    flex: 1,
    borderTopWidth: 1,
    borderColor: 'white',
    paddingTop: 10,
    alignItems: 'center',
  },
});
export default playerList;
