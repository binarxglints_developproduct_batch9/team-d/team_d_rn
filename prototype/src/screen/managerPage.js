import React from 'react';
import {Text, View, TouchableOpacity, StyleSheet} from 'react-native';
import {removeValue} from '../helper/asyncStorageToken';
import {logoutAction} from '../actions/authAction';
import {useDispatch, useSelector} from 'react-redux';

const managerPage = () => {
  const dispatch = useDispatch();
  return (
    <View style={style.container}>
      <View style={style.header}>

      </View>
      <Text>managerPage</Text>
      {/* <TouchableOpacity
        onPress={async () => {
          removeValue();
          dispatch(logoutAction());
        }}>
        <Text>emergency exit</Text>
      </TouchableOpacity> */}
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#313131',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default managerPage;
