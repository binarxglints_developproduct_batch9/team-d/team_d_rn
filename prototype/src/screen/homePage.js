import React, {useEffect, useState} from 'react';
import {
  View,
  Image,
  FlatList,
  TouchableOpacity,
  Modal,
  ActivityIndicator,
  Text,
} from 'react-native';
import FieldCard from '../components/FieldCard';
import {useDispatch, useSelector} from 'react-redux';
import {getFieldAction, getFieldCity} from '../actions/fieldAction';
import {userAction} from '../actions/userAction';
import Icon from 'react-native-vector-icons/FontAwesome';
import FilterModal from '../components/FilterModal';
import {ActionSheetIOS} from 'react-native';

const homePage = ({navigation}) => {
  const city = [
    'All',
    'Jakarta',
    'Bandung',
    'Semarang',
    'Tangerang',
    'Surabaya',
  ];
  const [locationVisible, setLocationVisible] = useState(false);
  const [filterVisible, setFilterVisible] = useState(false);
  const [loading, setLoading] = useState(true);
  const [loadingCity, setLoadingCity] = useState(false);
  const listField = useSelector((state) => state.field);
  const auth = useSelector((state) => state.auth);
  const userList = useSelector((state) => state.user);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getFieldAction()).then(() => setLoading(false));
  }, [auth.payload]);

  useEffect(() => {}, [auth]);

  useEffect(() => {
    dispatch(userAction({token: auth.payload}));
  }, []);

  if (loading == true) {
    return (
      <View style={style.loading}>
        <ActivityIndicator size="large" color="#28df99" />
      </View>
    );
  }

  return (
    <View style={style.container}>
      <View style={style.titleContainer}>
        <Image
          style={style.logo}
          source={{
            uri:
              'https://media.discordapp.net/attachments/795872155531214868/796383487249612841/Drawing_3.png?width=943&height=462',
          }}
        />
        <View style={style.titleSubContainer2}>
          <TouchableOpacity
            onPress={() => {
              setFilterVisible(true);
            }}>
            <Icon
              name="filter"
              size={24}
              color={'#28df99'}
              style={{marginRight: 15}}
            />
          </TouchableOpacity>
          <Modal
            animationType="fade"
            transparent={true}
            visible={filterVisible}
            onRequestClose={() => {
              setFilterVisible(false);
            }}>
            <View style={style.modalContainer}>
              <FilterModal setFilterVisible={setFilterVisible} />
            </View>
          </Modal>
          <Icon
            name="search"
            size={24}
            color={'#28df99'}
            onPress={() => navigation.navigate('searchField')}
          />
        </View>
      </View>
      <View style={style.fieldContainer}>
        <View
          style={{
            paddingLeft: 10,
            paddingVertical: 10,
            borderColor: 'white',
            borderBottomWidth: 1,
          }}>
          <FlatList
            showsHorizontalScrollIndicator={false}
            horizontal
            data={city}
            keyExtractor={(item) => item}
            renderItem={({item}) => {
              return (
                <TouchableOpacity
                  onPress={async () => {
                    setLoadingCity(true);
                    if (item === 'All') {
                      dispatch(getFieldAction()).then(() =>
                        setLoadingCity(false),
                      );
                    } else {
                      dispatch(getFieldCity(item)).then(() => {
                        setLoadingCity(false);
                      });
                    }
                  }}>
                  <Text style={style.city}>{item}</Text>
                </TouchableOpacity>
              );
            }}
          />
        </View>

        {loadingCity ? (
          <View style={style.loading}>
            <ActivityIndicator size="large" color="#28df99" />
          </View>
        ) : (
          <FlatList
            showsVerticalScrollIndicator={false}
            data={listField.listField}
            renderItem={({item}) => {
              return (
                <FieldCard
                  id={item.id}
                  name={item.fieldName}
                  location={item.location}
                  image={item.image}
                  price={item.price}
                  description={item.description}
                  city={item.city}
                  detailField={() =>
                    navigation.navigate('homeDetails', {
                      id: item.id,
                      name: item.fieldName,
                      location: item.location,
                      image: item.image,
                      price: item.price,
                      description: item.description,
                      city: item.city,
                    })
                  }
                />
              );
            }}
            keyExtractor={(item) => item.id}
          />
        )}
      </View>
    </View>
  );
};

const style = {
  loading: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#313131',
  },
  container: {
    flex: 1,
    backgroundColor: '#313131',
    flexDirection: 'column',
    alignItems: 'center',
  },
  titleContainer: {
    width: '90%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: '8%',
  },
  titleSubContainer2: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  //City
  city: {
    borderWidth: 1,
    borderColor: '#28df99',
    padding: 10,
    color: 'white',
    marginRight: 10,
    borderRadius: 7,
  },
  modalContainer: {
    alignItems: 'center',
    top: '7%',
  },
  titleList: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 24,
  },
  logo: {
    width: '25%',
    height: null,
    aspectRatio: 2,
  },
  fieldContainer: {
    flex: 1,
    width: '100%',
    borderTopWidth: 1,
    borderColor: 'white',
    justifyContent: 'center',
  },
};

export default homePage;
