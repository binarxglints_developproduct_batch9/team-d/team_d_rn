import React, {useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ToastAndroid,
  Modal,
} from 'react-native';
import {removeValue} from '../helper/asyncStorageToken';
import {logoutAction} from '../actions/authAction';
import {BASE_URL} from '../constant/general';
import {useDispatch, useSelector} from 'react-redux';

const profilePage = ({navigation}) => {
  const [profileVisible, setProfileVisible] = useState(false);
  const userList = useSelector((state) => state.user);
  const dispatch = useDispatch();

  return (
    <View style={style.container}>
      {/* <HeaderProfile /> */}
      <View style={style.subContainer1}>
        <TouchableOpacity
          onPress={() => {
            setProfileVisible(true);
          }}>
          <Image
            style={style.image}
            source={{uri: `${BASE_URL}/${userList.userList.profilePic}`}}
          />
        </TouchableOpacity>
        <Modal
          animationType="fade"
          transparent={true}
          visible={profileVisible}
          onRequestClose={() => {
            setProfileVisible(false);
          }}>
          <View style={style.modalContainerImage}>
            <Image
              style={style.modalImage}
              source={{uri: `${BASE_URL}/${userList.userList.profilePic}`}}
            />
          </View>
        </Modal>
        <Text style={style.nameText}>{userList.userList.fullname}</Text>
        <View style={style.bioContainer}>
          <Text style={style.subText}>Bio</Text>
          <Text style={style.bioText}>{userList.userList.description}</Text>
          <Text style={style.subText}>No Handphone</Text>
          <Text style={style.bioText}>{userList.userList.phone}</Text>
        </View>
      </View>
      <View style={style.subContainer2}>
        <TouchableOpacity
          style={style.editButton}
          onPress={() => navigation.navigate('editProfile')}>
          <Text style={style.editText}>Edit Profile</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={style.signoutButton}
          onPress={async () => {
            removeValue();
            dispatch(logoutAction());
            ToastAndroid.show(`Good Bye ${userList.userList.fullname}!`, 2000);
          }}>
          <Text style={style.signoutText}>Sign Out</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#313131',
    alignItems: 'center',
  },
  subContainer1: {
    height: '75%',
    width: '100%',
    alignItems: 'center',
    paddingTop: 50,
  },
  image: {
    width: '30%',
    height: null,
    aspectRatio: 1,
    marginBottom: 15,
    borderRadius: 100,
    borderWidth: 2,
    borderColor: '#28df99',
  },
  modalContainerImage: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    backgroundColor: '#313131',
  },
  modalImage: {
    width: '100%',
    height: null,
    aspectRatio: 1,
  },
  nameText: {
    color: '#28df99',
    textAlign: 'center',
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 50,
  },
  bioContainer: {
    borderWidth: 1,
    borderColor: '#545454',
    width: '90%',
    borderRadius: 7,
    alignItems: 'center',
    padding: 10,
  },
  subText: {
    color: '#28df99',
    width: '100%',
    fontWeight: 'bold',
    fontSize: 16,
    marginBottom: 2,
    textAlign: 'justify',
  },
  bioText: {
    color: 'white',
    width: '100%',
    marginBottom: 10,
    textAlign: 'justify',
  },
  subContainer2: {
    height: '25%',
    alignItems: 'center',
    width: '100%',
    justifyContent: 'flex-end',
    paddingBottom: 40,
  },
  editButton: {
    width: '90%',
    borderRadius: 7,
    // backgroundColor: '#28df99',
    alignItems: 'center',
    padding: 10,
    marginBottom: 10,
    borderColor: '#28df99',
    borderWidth: 1,
  },
  signoutButton: {
    width: '90%',
    borderRadius: 7,
    // backgroundColor: 'red',
    alignItems: 'center',
    padding: 10,
    borderColor: 'red',
    borderWidth: 1,
  },
  editText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white',
  },
  signoutText: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
  },
});

export default profilePage;