import React, {useState, useEffect} from 'react';
import {Text, View, StyleSheet, TouchableOpacity, Modal} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import {getBookingAction} from '../actions/bookAction';
import QRCode from 'react-native-qrcode-svg';

const confirmationPage = ({route}) => {
  const [ticketVisible, setTicketVisible] = useState(false);
  const auth = useSelector((state) => state.auth);
  const book = useSelector((state) => state.book);
  const dispatch = useDispatch();
  const navigation = useNavigation();
  const [picked, setPicked] = useState([]);
  let data = {};
  const handleBooking = () => {
    for (let i = 0; i < picked.length; i++) {
      data[`id_timeslot[${i}]`] = picked[i].index;
    }
    dispatch(
      getBookingAction({
        token: auth.payload,
        date: date,
        id: route.params.id,
        ...data,
      }),
    );
  };
  useEffect(() => {
    let filtered = route.params.timeSlotPicked.filter((el) => {
      if (el.status == true) {
        return el;
      }
    });
    setPicked(filtered);
  }, []);

  useEffect(() => {}, [picked]);
  let date = `${route.params.date.getFullYear()}-${
    route.params.date.getMonth() + 1
  }-${route.params.date.getDate()}`;

  console.log('confirm book', book);

  return (
    <View style={style.container}>
      <View style={style.subContainer}>
        <Text style={style.checkText}>Check Your Booking</Text>
        <View style={style.checkContainer}>
          <Text style={style.dateText}>Field :</Text>
          <Text style={style.dateSubText}>{route.params.name}</Text>
          <Text style={style.dateText}>Date :</Text>
          <Text style={style.dateSubText}>{date}</Text>
          <Text style={style.dateText}>Time Slot :</Text>
          {picked.length != 0 &&
            picked.map((pick, index) => {
              return (
                <Text key={index} style={style.dateSubText}>
                  {pick.time}
                </Text>
              );
            })}
          <Text style={style.dateText}>Price :</Text>
          <Text style={style.dateSubText}>
            {picked.length * route.params.price} K
          </Text>
        </View>
        <TouchableOpacity
          onPress={() => {
            handleBooking();
            setTicketVisible(true);
          }}>
          <Text style={style.submitButton}>Submit</Text>
        </TouchableOpacity>
      </View>
      <Modal
        animationType="fade"
        transparent={true}
        visible={ticketVisible}
        onRequestClose={() => {
          setTicketVisible(false);
          navigation.navigate('homePage');
        }}>
        <View style={style.container}>
          <View style={style.subContainer}>
            <Text style={style.checkText}>Booking Complete!</Text>
            <Text style={style.bookText1}>Your Booking ID :</Text>
            <QRCode size={200} value={book.booked.id} />
            <Text style={style.bookText2}>{book.booked.id}</Text>
            <Text style={style.bookText3}>
              Please show this Booking ID at the Field Registration section
            </Text>
            <TouchableOpacity
              onPress={() => {
                setTicketVisible(false);
                navigation.navigate('homepage');
              }}
              style={style.finalButton}>
              <Text style={style.backButton}>Back to Home</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    backgroundColor: '#313131',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  subContainer: {
    borderWidth: 1,
    width: '90%',
    borderColor: '#28df99',
    borderRadius: 10,
    alignItems: 'center',
    padding: 20,
  },
  checkText: {
    color: '#FFCB74',
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  checkContainer: {
    borderWidth: 1,
    borderColor: '#545454',
    borderRadius: 7,
    paddingBottom: 15,
    width: '100%',
    alignItems: 'center',
    marginBottom: 20,
  },
  dateText: {
    color: '#28df99',
    fontWeight: 'bold',
    marginTop: 15,
  },
  dateSubText: {
    color: 'white',
    fontSize: 16,
  },
  timeSubText: {
    color: 'white',
    fontSize: 16,
  },
  submitButton: {
    borderWidth: 1,
    borderColor: '#28df99',
    padding: 10,
    color: 'white',
    fontWeight: 'bold',
    borderRadius: 7,
  },
  //Modal
  bookText1: {
    color: '#28df99',
    marginTop: 50,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  bookText2: {
    color: 'white',
    marginTop: 10,
    marginBottom: 50,
    fontWeight: 'bold',
    fontSize: 16,
  },
  bookText3: {
    color: 'white',
    textAlign: 'center',
  },
  backButton: {
    color: 'white',
    borderWidth: 1,
    borderColor: '#545454',
    borderRadius: 7,
    padding: 10,
    marginTop: 20,
  },
});

export default confirmationPage;
