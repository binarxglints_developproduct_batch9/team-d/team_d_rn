import React, { useState, useEffect } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Image,
  TextInput,
  ToastAndroid,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import {
  requestMultiple,
  PERMISSIONS,
  checkMultiple,
} from 'react-native-permissions';
import { updateProfileAction } from '../actions/userAction';
import FormData from 'form-data';
import ImagePicker from 'react-native-image-picker';
import { ActivityIndicator } from 'react-native';

const editProfile = ({ navigation }) => {
  const [image, setImage] = useState({});
  const [fullname, setFullname] = useState("");
  const [bio, setBio] = useState("");
  const [Handphone, setHandphone] = useState("");
  const auth = useSelector((state) => state.auth);
  const userList = useSelector((state) => state.user);
  const [loading, setLoading] = useState(false)
  const dispatch = useDispatch();
  const submitProfile = () => {
    const data = new FormData();
    data.append('fullname', fullname);
    data.append('description', bio);
    data.append('profilePic', { ...image });
    data.append('phone', Handphone);
    dispatch(updateProfileAction({ data, token: auth.payload }));
  };

  const checkPermission = () => {
    checkMultiple([
      PERMISSIONS.ANDROID.CAMERA,
      PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,
    ]).then((statuses) => {
      if (
        statuses[PERMISSIONS.ANDROID.CAMERA] !== 'granted' ||
        statuses[PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE] !== 'granted'
      ) {
        requestPermission();
      } else {
        handleImagePicker(image);
      }
    });
  };
  const requestPermission = () => {
    requestMultiple([
      PERMISSIONS.ANDROID.CAMERA,
      PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,
    ]).then((statuses) => {
      if (
        statuses[PERMISSIONS.ANDROID.CAMERA] === 'granted' ||
        statuses[PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE] === 'granted'
      ) {
        handleImagePicker();
      }
    });
  };
  const handleImagePicker = () => {
    const options = {
      title: 'Choose photo...',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        1;
      } else {
        // setImage('data:image/jpeg;base64,' + response.data);
        // console.log(response);
        if (response.uri) {
          setImage({
            type: response.type,
            uri: response.uri,
            name: response.fileName,
          });
        }
      }
    });
  };

  return (
    <View style={style.container}>
      <TouchableOpacity onPress={() => checkPermission()}>
        <Image
          style={style.image}
          source={{
            uri:
              image.uri ||
              'https://cdn.business2community.com/wp-content/uploads/2017/08/blank-profile-picture-973460_640.png',
          }}
        />
      </TouchableOpacity>
      <Text style={style.subText}>Full Name</Text>
      <TextInput
        placeholder={userList.userList.fullname}
        placeholderTextColor={'gray'}
        style={style.inputText}
        onChangeText={(fullname) => setFullname(fullname)}
        value={fullname}
      />
      <Text style={style.subText}>Bio</Text>
      <TextInput
        placeholder={userList.userList.description}
        placeholderTextColor={'gray'}
        style={style.inputText2}
        multiline
        onChangeText={(bio) => setBio(bio)}
        value={bio}
      />
      <Text style={style.subText}>No. Handphone</Text>
      <TextInput
        placeholder={userList.userList.phone}
        placeholderTextColor={'gray'}
        style={style.inputText}
        onChangeText={(Handphone) => setHandphone(Handphone)}
        value={Handphone}
      />
      <TouchableOpacity
        style={style.loginButton}
        onPress={async () => {
          if (
            fullname === "" || bio === "" || Handphone === ""
          ) { ToastAndroid.show("You must fill all fields", 2000) }
          else {
            setLoading(true)
            await submitProfile()
            setLoading(false)
            navigation.navigate('Profile')
            ToastAndroid.show(`Profile Updated`, 2000)
          }
        }}>
        {loading ? <View>
          <ActivityIndicator size="large" color="#28df99" />
        </View> :
          <Text style={style.textLogin}>Update</Text>
        }
      </TouchableOpacity>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#313131',
    alignItems: 'center',
    width: '100%',
  },
  image: {
    width: '30%',
    height: null,
    aspectRatio: 1,
    marginBottom: 25,
    borderRadius: 100,
    marginTop: 50,
    borderColor: '#28df99',
    borderWidth: 2,
  },
  subText: {
    color: '#28df99',
    width: '90%',
    fontWeight: 'bold',
    fontSize: 16,
    marginBottom: 5,
  },
  inputText: {
    width: '90%',
    borderRadius: 7,
    borderWidth: 1,
    borderColor: '#545454',
    color: 'white',
    padding: 10,
    marginBottom: 10,
  },
  inputText2: {
    width: '90%',
    height: 100,
    borderRadius: 7,
    color: 'white',
    padding: 10,
    marginBottom: 10,
    textAlignVertical: 'top',
    borderWidth: 1,
    borderColor: '#545454',
  },
  loginButton: {
    width: '90%',
    borderRadius: 7,
    alignItems: 'center',
    padding: 10,
    marginTop: 10,
    borderWidth: 1,
    borderColor: '#28df99',
  },
  textLogin: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
});
export default editProfile;
