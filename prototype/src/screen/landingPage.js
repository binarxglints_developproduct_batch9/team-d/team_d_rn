import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  Image,
  StatusBar,
  ImageBackground,
} from 'react-native';
import {useDispatch} from 'react-redux';

const landingPage = ({navigation}) => {
  setTimeout(() => {
    navigation.replace('loginPage');
  }, 3000);
  return (
    <View style={style.container}>
      <StatusBar hidden={true} />
      <ImageBackground
        source={{
          uri: 'https://i.ibb.co/19nN3nZ/soka.jpg',
        }}
        style={style.imageBackground}>
        <View style={style.subContainer}>
          <Image
            style={style.logo}
            source={{
              uri:
                'https://media.discordapp.net/attachments/795872155531214868/796383487249612841/Drawing_3.png?width=943&height=462',
            }}
          />
          <Text style={style.branding}>New Challenges Ahead!</Text>
        </View>
      </ImageBackground>
    </View>
  );
};
const style = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#313131',
  },
  imageBackground: {
    flex: 1,
    resizeMode: 'cover',
    justifyContent: 'center',
  },
  subContainer: {
    top: '65%',
    position: 'absolute',
    width: '100%',
    alignItems: 'center',
  },
  logo: {
    width: '50%',
    height: null,
    aspectRatio: 2,
  },
  branding: {
    color: 'white',
    fontSize: 15,
  },
});

export default landingPage;
