import React, {useEffect, useState} from 'react';
import {
  Text,
  Image,
  View,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {FIELD_API} from '../constant/general';
import {useDispatch, useSelector} from 'react-redux';
import {getFeedbackAction} from '../actions/feedbackAction';
import {getRatingAction} from '../actions/ratingAction';
import FeedbackCard from '../components/FeedbackCard';

const homeDetails = ({route}) => {
  const navigation = useNavigation();
  const auth = useSelector((state) => state.auth);
  const feedbackList = useSelector((state) => state.feedback);
  const ratingList = useSelector((state) => state.rating);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getFeedbackAction({token: auth.payload, id: route.params.id}));
  }, []);

  useEffect(() => {
    dispatch(getRatingAction({token: auth.payload, id: route.params.id}));
  }, []);

  console.log('rating homedetail', ratingList.rating);

  return (
    <View style={style.container}>
      <View style={{width: '100%'}}>
        <ScrollView horizontal style={{width: '100%', height: 'auto'}}>
          {route.params.image &&
            route.params.image.map((imgUrl, idx) => (
              <Image
                key={idx}
                source={{uri: `${FIELD_API}/${imgUrl}`}}
                style={{
                  height: 175,
                  width: null,
                  aspectRatio: 1.78,
                  marginRight: 7,
                }}
              />
            ))}
        </ScrollView>
      </View>
      <View style={style.title}>
        <Text style={style.titleText}>{route.params.name}</Text>
        <Text style={style.priceText}>
          {route.params.price.$numberDecimal} K
        </Text>
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{paddingBottom: 15}}>
          <Text style={style.descriptionText}>City</Text>
          <Text style={style.detailsText}>{route.params.city}</Text>
          <Text style={style.descriptionText}>Location</Text>
          <Text style={style.detailsText}>{route.params.location}</Text>
          <Text style={style.descriptionText}>Description</Text>
          <Text style={style.detailsText}>{route.params.description}</Text>
        </View>
      </ScrollView>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          borderTopWidth: 1,
          borderColor: 'white',
        }}>
        <Text style={style.feedbackSubText}>Feedback</Text>
        <View style={style.rating}>
          <Image
            source={{
              uri:
                'https://www.freepnglogos.com/uploads/star-png/star-vector-png-transparent-image-pngpix-21.png',
            }}
            style={style.star}
          />
          <Text style={style.ratingText}>{ratingList.rating} / 5</Text>
        </View>
      </View>
      {feedbackList.feedback.length === 0 ? (
        <Text style={style.feedback}>No Feedback Available</Text>
      ) : (
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          style={style.flatlist}
          showsVerticalScrollIndicator={false}
          data={feedbackList.feedback}
          renderItem={({item}) => {
            return (
              <FeedbackCard
                username={item.username}
                email={item.email}
                field={item.field}
                id_booking={item.id_booking}
                rating={item.rating}
                review={item.review}
                _id={item._id}
              />
            );
          }}
          keyExtractor={(item) => item._id}
        />
      )}
      <View style={style.footer}>
        <TouchableOpacity
          style={style.bookButton}
          onPress={() => {
            navigation.navigate('bookingPage', {
              id: route.params.id,
              name: route.params.name,
              price: route.params.price.$numberDecimal,
            });
          }}>
          <Text style={style.bookText}>Book!</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const style = {
  container: {
    flex: 1,
    backgroundColor: '#313131',
    width: '100%',
  },
  image: {
    width: '100%',
    height: null,
    aspectRatio: 1.78,
  },
  title: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: 'white',
    paddingVertical: 15,
    paddingHorizontal: '5%',
  },
  titleText: {
    color: '#28df99',
    fontWeight: 'bold',
    fontSize: 20,
  },
  priceText: {
    color: '#FFCB74',
    fontWeight: 'bold',
    fontSize: 20,
  },
  descriptionText: {
    color: '#28df99',
    fontWeight: 'bold',
    fontSize: 16,
    marginTop: 15,
    paddingHorizontal: '5%',
  },
  detailsText: {
    color: 'white',
    textAlign: 'justify',
    paddingHorizontal: '5%',
  },
  //Feedback Section
  feedback: {
    color: 'white',
    paddingLeft: '5%',
    paddingBottom: 15,
  },
  feedbackSubText: {
    color: '#28df99',
    fontWeight: 'bold',
    fontSize: 16,
    paddingLeft: '5%',
  },
  rating: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
  },
  star: {
    height: 20,
    width: 20,
    marginHorizontal: 10,
  },
  ratingText: {
    color: 'white',
    fontSize: 15,
  },
  //Feedback List
  flatlist:{
    width: '90%',
    marginHorizontal: '5%',
    marginBottom: 15,
  },
  feedbackCard: {
    alignItems: 'center',
    paddingHorizontal: '5%',
  },
  feedbackList: {
    width: '100%',
    height: 'auto',
    marginBottom: 15,
    borderRadius: 7,
    padding: 20,
    borderWidth: 1,
    borderColor: '#545454',
  },
  userText: {
    color: '#28df99',
    fontWeight: 'bold',
  },
  starList: {
    flexDirection: 'row',
    marginVertical: 5,
  },
  starRating: {
    marginRight: 5,
    width: 20,
    height: 20,
  },
  feedbackText: {
    color: 'white',
    textAlign: 'justify',
  },
  //Footer
  footer: {
    width: '100%',
    height: 'auto',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingVertical: 15,
    borderTopWidth: 1,
    borderColor: 'white',
  },
  playerButton: {
    borderRadius: 7,
    marginRight: '5%',
    width: '42.5%',
    borderWidth: 1,
    borderColor: 'white',
  },
  bookButton: {
    borderRadius: 7,
    width: '90%',
    borderWidth: 1,
    borderColor: '#28df99',
  },
  playerText: {
    color: '#28df99',
    fontWeight: 'bold',
    padding: 10,
    textAlign: 'center',
  },
  bookText: {
    color: 'white',
    fontWeight: 'bold',
    padding: 10,
    textAlign: 'center',
  },
};

export default homeDetails;
