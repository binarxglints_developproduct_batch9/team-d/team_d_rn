import * as types from '../constant/actionType';

const initialState = {
  loading: false,
  feedback: {},
  error: null,
};

function createFeedbackReducer(state = initialState, action) {
  switch (action.type) {
    case types.FEEDBACK_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });
    case types.FEEDBACK_SUCCESS:
      // console.log(action.payload);
      return {
        loading: false, 
        feedback: action.payload
      }
    case types.FEEDBACK_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        error: action.error,
      });
    default:
      return state;
  }
}

export default createFeedbackReducer;
