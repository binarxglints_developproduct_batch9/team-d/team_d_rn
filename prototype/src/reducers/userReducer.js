import * as types from '../constant/actionType';

const initialState = {
  loading: false,
  userList: {},
  error: null,
};

function userReducer(state = initialState, action) {
  switch (action.type) {
    case types.USER_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });
    case types.USER_SUCCESS:
      // console.log(action.payload);
      return Object.assign({}, state, {
        loading: false,
        userList: action.payload,
      });
    case types.USER_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        error: action.error,
      });
    case types.UPDATE_PROFILE_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });
    case types.UPDATE_PROFILE_SUCCESS:
      console.log('ini update patch baru', action.payload);
      return Object.assign({}, state, {
        loading: false,
        userList: action.payload,
      });
    case types.UPDATE_PROFILE_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        error: action.error,
      });
    default:
      return state;
  }
}

export default userReducer;
