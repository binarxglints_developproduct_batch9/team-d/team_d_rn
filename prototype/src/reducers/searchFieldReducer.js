import * as types from '../constant/actionType';

const initialState = {
    loading: false,
    listField: [],
    error: null,
};

function SearchFieldReducer(state = initialState, action) {
    switch (action.type) {
        case types.SEARCH_FIELD_REQUEST:
            return Object.assign({}, state, {
                loading: true,
            });
        case types.SEARCH_FIELD_SUCCESS:
            // console.log(action.payload);
            return Object.assign({}, state, {
                loading: false,
                listField: action.payload,
            });
        case types.SEARCH_FIELD_FAILURE:
            return Object.assign({}, state, {
                loading: false,
                error: action.error,
            });
        default:
            return state;
    }
}

export default SearchFieldReducer;