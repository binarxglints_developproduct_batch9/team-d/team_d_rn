import {combineReducers} from 'redux';
import authReducers from './authReducer';
import fieldReducers from './fieldReducer';
import searchFieldReducer from './searchFieldReducer';
import userReducer from './userReducer';
import timeSlotReducer from './timeSlotReducer';
import feedbackReducer from './feedbackReducer';
import bookReducer from './bookReducer';
import historyReducer from './historyReducer';
import createFeedbackReducer from './createFeedbackReducer';
import ratingReducer from './ratingReducer';

const rootReducer = combineReducers({
  auth: authReducers,
  field: fieldReducers,
  search: searchFieldReducer,
  user: userReducer,
  time: timeSlotReducer,
  feedback: feedbackReducer,
  book: bookReducer,
  history: historyReducer,
  createFeedback: createFeedbackReducer,
  rating: ratingReducer,
});

export default rootReducer;
