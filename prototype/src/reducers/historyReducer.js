import * as types from '../constant/actionType';

const initialState = {
  loading: false,
  history: {},
  error: null,
};

function historyReducer(state = initialState, action) {
  switch (action.type) {
    case types.HISTORY_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });
    case types.HISTORY_SUCCESS:
      // console.log(action.payload);
      return {
        loading: false, 
        history: action.payload
      }
    case types.HISTORY_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        error: action.error,
      });
    default:
      return state;
  }
}

export default historyReducer;
