import * as types from '../constant/actionType';

const initialState = {
  loading: false,
  booked: {},
  error: null,
};

function bookReducer(state = initialState, action) {
  switch (action.type) {
    case types.BOOKING_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });
    case types.BOOKING_SUCCESS:
      // console.log(action.payload);
      return {
        loading: false, 
        booked: action.payload
      }
    case types.BOOKING_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        error: action.error,
      });
    default:
      return state;
  }
}

export default bookReducer;
