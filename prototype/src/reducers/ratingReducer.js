import * as types from '../constant/actionType';

const initialState = {
  loading: false,
  rating: [],
  error: null,
};

function ratingReducer(state = initialState, action) {
  switch (action.type) {
    case types.RATING_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });
    case types.RATING_SUCCESS:
      // console.log(action.payload);
      return {
        loading: false, 
        rating: action.payload
      }
    case types.RATING_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        error: action.error,
      });
    default:
      return state;
  }
}

export default ratingReducer;
