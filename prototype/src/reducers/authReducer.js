import * as types from '../constant/actionType';

const initialState = {
  loading: false,
  payload: '',
  role: '',
  error: null,
};

function authReducer(state = initialState, action) {
  switch (action.type) {
    //Case For Handle Login
    case types.LOGIN_REQUEST:
      console.log(action);
      return Object.assign({}, state, {
        loading: true,
      });
    case types.LOGIN_SUCCESS:
      console.log(action.payload);
      return Object.assign({}, state, {
        loading: false,
        payload: action.payload.token, role: action.payload.role
      });
    case types.LOGIN_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        error: action.error,
      });

    case types.REGISTER_REQUEST:
      console.log(action);
      return Object.assign({}, state, {
        loading: true,
      });
    case types.REGISTER_SUCCESS:
      console.log(action.payload);
      return Object.assign({}, state, {
        loading: false,
        payload: action.payload,
      });
    case types.REGISTER_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        error: action.error,
      });
    case types.LOGOUT_SUCCESS:
      console.log('logOut success triggered');
      return {
        loading: false,
        payload: '',
        error: null,
      };
    default:
      return state;
  }
}

export default authReducer;
