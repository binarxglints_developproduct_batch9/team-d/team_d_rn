import * as types from '../constant/actionType';

const initialState = {
  loading: false,
  listField: [],
  error: null,
};

function fieldReducer(state = initialState, action) {
  switch (action.type) {
    case types.GET_FIELD_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });
    case types.GET_FIELD_SUCCESS:
      // console.log(action.payload);
      return Object.assign({}, state, {
        loading: false,
        listField: action.payload,
      });
    case types.GET_FIELD_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        error: action.error,
      });
    default:
      return state;
  }
}

export default fieldReducer;
