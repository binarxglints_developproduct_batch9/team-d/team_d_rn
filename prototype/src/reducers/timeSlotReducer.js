import * as types from '../constant/actionType';

const initialState = {
  loading: false,
  timeSlot: [],
  error: null,
};

function timeSlotReducer(state = initialState, action) {
  switch (action.type) {
    case types.TIME_SLOT_REQUEST:
      return Object.assign({}, state, {
        loading: true,
      });
    case types.TIME_SLOT_SUCCESS:
      // console.log(action.payload);
      return Object.assign({}, state, {
        loading: false,
        timeSlot: action.payload,
      });
    case types.TIME_SLOT_FAILURE:
      return Object.assign({}, state, {
        loading: false,
        error: action.error,
      });
    default:
      return state;
  }
}

export default timeSlotReducer;
