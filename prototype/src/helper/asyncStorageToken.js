import AsyncStorage from '@react-native-async-storage/async-storage';

// export const storeDataKey = async (value) => {
//   try {
//     await AsyncStorage.setItem('@storage_Key', value);
//   } catch (e) {
//     console.log(e);
//   }
// };

// export const getDataKey = async () => {
//   try {
//     const value = await AsyncStorage.getItem('@storage_Key');
//     if (value !== null) {
//       // value previously stored
//       return value;
//     }
//   } catch (e) {
//     // error reading value
//     console.log(e);
//   }
// };

export const removeValue = async () => {
  try {
    await AsyncStorage.removeItem('tok');
    await AsyncStorage.removeItem('role');
  } catch (e) {
    // remove error
  }
};
