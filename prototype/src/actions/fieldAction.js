import * as types from '../constant/actionType';
import axios from 'axios';
import {FIELD_API} from '../constant/general';

export const getFieldRequest = () => ({
  type: types.GET_FIELD_REQUEST,
});

export const getFieldSuccess = (fields) => ({
  type: types.GET_FIELD_SUCCESS,
  payload: fields,
});

export const getFieldFailure = (error) => ({
  type: types.GET_FIELD_FAILURE,
  error,
});

export const getFieldAction = () => {
  return async (dispatch) => {
    try {
      dispatch(getFieldRequest);
      const res = await axios.get(`${FIELD_API}/field`);
      const filtered = [];
      let images = [];
      res.data.data.forEach((field) => {
        images = [];
        field.image.forEach((img) => {
          // img = img.slice(7);
          img = img;
          images.push(img);
        });
        field.image = images;
        filtered.push(field);
      });
      dispatch(getFieldSuccess(filtered));
    } catch (error) {
      dispatch(getFieldFailure(error));
    }
  };
};

export const getFieldActionPriceAsc = () => {
  return async (dispatch) => {
    try {
      dispatch(getFieldRequest);
      const res = await axios.get(`${FIELD_API}/field/?sortByPrice=asc`);
      const filtered = [];
      let images = [];
      res.data.data.forEach((field) => {
        images = [];
        field.image.forEach((img) => {
          // img = img.slice(7);
          img = img;
          images.push(img);
        });
        field.image = images;
        filtered.push(field);
      });
      dispatch(getFieldSuccess(filtered));
    } catch (error) {
      dispatch(getFieldFailure(error));
    }
  };
};

export const getFieldActionPriceDesc = () => {
  return async (dispatch) => {
    try {
      dispatch(getFieldRequest);
      const res = await axios.get(`${FIELD_API}/field/?sortByPrice=desc`);
      const filtered = [];
      let images = [];
      res.data.data.forEach((field) => {
        images = [];
        field.image.forEach((img) => {
          // img = img.slice(7);
          img = img;
          images.push(img);
        });
        field.image = images;
        filtered.push(field);
      });
      dispatch(getFieldSuccess(filtered));
    } catch (error) {
      dispatch(getFieldFailure(error));
    }
  };
};

export const getFieldActionNameAsc = () => {
  return async (dispatch) => {
    try {
      dispatch(getFieldRequest);
      const res = await axios.get(`${FIELD_API}/field/?sortByField=asc`);
      const filtered = [];
      let images = [];
      res.data.data.forEach((field) => {
        images = [];
        field.image.forEach((img) => {
          // img = img.slice(7);
          img = img;
          images.push(img);
        });
        field.image = images;
        filtered.push(field);
      });
      dispatch(getFieldSuccess(filtered));
    } catch (error) {
      dispatch(getFieldFailure(error));
    }
  };
};

export const getFieldActionNameDesc = () => {
  return async (dispatch) => {
    try {
      dispatch(getFieldRequest);
      const res = await axios.get(`${FIELD_API}/field/?sortByField=desc`);
      const filtered = [];
      let images = [];
      res.data.data.forEach((field) => {
        images = [];
        field.image.forEach((img) => {
          // img = img.slice(7);
          img = img;
          images.push(img);
        });
        field.image = images;
        filtered.push(field);
      });
      dispatch(getFieldSuccess(filtered));
    } catch (error) {
      dispatch(getFieldFailure(error));
    }
  };
};

export const getFieldCity = (city) => {
  return async (dispatch) => {
    try {
      dispatch(getFieldRequest);
      const res = await axios.get(`${FIELD_API}/field?city=${city}`);
      const filtered = [];
      let images = [];
      res.data.data.forEach((field) => {
        images = [];
        field.image.forEach((img) => {
          // img = img.slice(7);
          img = img;
          images.push(img);
        });
        field.image = images;
        filtered.push(field);
      });
      dispatch(getFieldSuccess(filtered));
    } catch (error) {
      dispatch(getFieldFailure(error));
    }
  };
};
