import * as types from '../constant/actionType';
import axios from 'axios';
import {BASE_URL, PROFILE_API} from '../constant/general';

//Handle PROFILE Login
export const userRequest = () => ({
  type: types.USER_REQUEST,
});

export const userSuccess = (payload) => ({
  type: types.USER_SUCCESS,
  payload,
});

export const userFailure = (error) => ({
  type: types.USER_FAILURE,
  error,
});

export const userAction = (payload) => {
  return async (dispatch) => {
    try {
      dispatch(userRequest());
      const res = await axios.get(`${BASE_URL}/user/profile`, {
        headers: {Authorization: `Bearer ${payload.token}`},
      });
      dispatch(userSuccess(res.data.data));
      return res.data.data
    } catch (error) {
      console.log('ini error', error);
      dispatch(userFailure(error));
    }
  };
};

export const updateProfileRequest = () => ({
  type: types.UPDATE_PROFILE_REQUEST,
});

export const updateProfileSuccess = (payload) => ({
  type: types.UPDATE_PROFILE_SUCCESS,
  payload,
});

export const updateProfileFailure = (error) => ({
  type: types.UPDATE_PROFILE_FAILURE,
  error,
});

export const updateProfileAction = (payload) => {
  return async (dispatch) => {
    try {
      dispatch(updateProfileRequest());
      const res = await axios.put(`${BASE_URL}/user/edit`, payload.data,{
        "headers": {"Authorization": `Bearer ${payload.token}`},
      });
      // console.log('update', res.data.data);
      
      dispatch(updateProfileSuccess(res.data.data));
    } catch (error) {
      console.log('ini error', error);
      dispatch(updateProfileFailure(error));
    }
  };
};
