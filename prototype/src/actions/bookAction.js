import * as types from '../constant/actionType';
import axios from 'axios';
import { TIMESLOT_API } from '../constant/general';

export const getBookingRequest = () => ({
    type: types.BOOKING_REQUEST,
});

export const getBookingSuccess = (booked) => ({
    type: types.BOOKING_SUCCESS,
    payload: booked,
});

export const getBookingFailure = (error) => ({
    type: types.BOOKING_FAILURE,
    error,
});

export const getBookingAction = (booked) => {
    console.log('booking action', booked)
    const slice = Object.keys(booked).slice(3).reduce((result, key) => {
        result[key] = booked[key];
        return result;
    }, {});
    let timeslot = []
    for (const key in slice) {
        //This will give key
        // console.log(key);
        // //This will give value
        // console.log(obj[key]);
        timeslot.push(slice[key])
    }
    let times = timeslot.toString()
    console.log('slicer', times);

    return async (dispatch) => {
        try {
            dispatch(getBookingRequest());
            let url = `${TIMESLOT_API}/booking/${booked.id}/create/bookedfield`
            const res = await axios.post(
                url,
                {
                    date: `${booked.date}`,
                    id_timeslot: times
                },
                {
                    headers: {
                        "Authorization": `Bearer ${booked.token}`,
                        "Content-Type": "application/json",
                    },
                }
            );
            console.log('respon booking', res.data.result);

            dispatch(getBookingSuccess(res.data.result));
        } catch (error) {
            console.log('Get booking Error', error);
            dispatch(getBookingFailure(error));
        }
    };
};
