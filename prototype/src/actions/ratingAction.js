import * as types from '../constant/actionType';
import axios from 'axios';
import { FEEDBACK_API } from '../constant/general';

export const getRatingRequest = () => ({
  type: types.RATING_REQUEST,
});

export const getRatingSuccess = (rating) => ({
  type: types.RATING_SUCCESS,
  payload: rating,
});

export const getRatingFailure = (error) => ({
  type: types.RATING_FAILURE,
  error,
});

export const getRatingAction = (rating) => {
  return async (dispatch) => {
    try {
      dispatch(getRatingRequest());
      let url = `${FEEDBACK_API}/feedback/rating/${rating.id}`
      const res = await axios.get(
        url,
        {
          headers: { 
            "Authorization": `Bearer ${rating.token}`,
            "Content-Type": "application/json",
           },
        }
      );
      console.log('respon rating', res.data.ratingAvg);
        dispatch(getRatingSuccess(res.data.ratingAvg));
    } catch (error) {
      console.log('Get Rating Error', error.response.data);
      dispatch(getRatingFailure(error));
    }
  };
};
