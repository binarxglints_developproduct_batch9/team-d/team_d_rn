import * as types from '../constant/actionType';
import axios from 'axios';
import { FEEDBACK_API } from '../constant/general';

export const createFeedbackRequest = () => ({
  type: types.CREATE_FEEDBACK_REQUEST,
});

export const createFeedbackSuccess = (feedback) => ({
  type: types.CREATE_FEEDBACK_SUCCESS,
  payload: feedback,
});

export const createFeedbackFailure = (error) => ({
  type: types.CREATE_FEEDBACK_FAILURE,
  error,
});

export const createFeedbackAction = (payload) => {
  console.log('------------>', payload);
  return async (dispatch) => {
    try {
      dispatch(createFeedbackRequest());
      let url = `${FEEDBACK_API}/feedback/${payload.id}/create`
      const res = await axios.post(
        url,
        {
          rating: payload.rating,
          review: payload.review,
        },
        {
          headers: {
            "Authorization": `Bearer ${payload.token}`,
            "Content-Type": "application/json",
          },
        },
      );
      console.log('!!!!!!!!!!!!!!!!!!!!!!', res);
        dispatch(createFeedbackSuccess(res.data.data));
    } catch (error) {
      console.log('Get Feedback Error', error.response.data);
      dispatch(createFeedbackFailure(error));
    }
  };
};
