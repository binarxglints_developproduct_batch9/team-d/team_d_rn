import * as types from '../constant/actionType';
import axios from 'axios';
import {FIELD_API} from '../constant/general';

//Handle Action Login
export const searchFieldRequest = () => ({
    type: types.SEARCH_FIELD_REQUEST,
  });
  
  export const searchFieldSuccess = (payload) => ({
    type: types.SEARCH_FIELD_SUCCESS,
    payload,
  });
  
  export const searchFieldFailure = (error) => ({
    type: types.SEARCH_FIELD_FAILURE,
    error,
  });
  
  export const searchFieldAction = (search) => {
    console.log(search);
    
    return async (dispatch) => {
      try {
        dispatch(searchFieldRequest());
        const res = await axios.get(`${FIELD_API}/field?fieldname=${search}`);
        dispatch(searchFieldSuccess(res.data.data));
      } catch (error) {
        console.log('ini error', error);
        dispatch(searchFieldFailure(error));
      }
    };
  };