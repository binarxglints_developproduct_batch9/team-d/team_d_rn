import * as types from '../constant/actionType';
import axios from 'axios';
import {TIMESLOT_API} from '../constant/general';

export const getHistoryRequest = () => ({
  type: types.HISTORY_REQUEST,
});

export const getHistorySuccess = (history) => ({
  type: types.HISTORY_SUCCESS,
  payload: history,
});

export const getHistoryFailure = (error) => ({
  type: types.HISTORY_FAILURE,
  error,
});

export const getHistoryAction = (payload) => {
  return async (dispatch) => {
    try {
      dispatch(getHistoryRequest());
      const res = await axios.get(`${TIMESLOT_API}/booking/history`, {
        headers: {Authorization: `Bearer ${payload.token}`},
      });
      
      dispatch(getHistorySuccess(res.data.data));
    } catch (error) {
      console.log('Get History Error', error);
      dispatch(getHistoryFailure(error));
    }
  };
};
