import * as types from '../constant/actionType';
import {TIMESLOT_API} from '../constant/general';
import axios from 'axios';
import qs from 'qs';

//Handle Action Login
export const timeSlotRequest = () => ({
  type: types.TIME_SLOT_REQUEST,
});

export const timeSlotSuccess = (payload) => ({
  type: types.TIME_SLOT_SUCCESS,
  payload,
});

export const timeSlotFailure = (error) => ({
  type: types.TIME_SLOT_FAILURE,
  error,
});

export const timeSlotAction = (payload) => {
  return async (dispatch) => {
    try {
      dispatch(timeSlotRequest());

      let url = `${TIMESLOT_API}/booking/${payload.id}/bookedfield`;
      const res = await axios({
        method: 'POST',
        url,
        data: {
          date: `${payload.date.getFullYear()}-${
            payload.date.getMonth() + 1
          }-${payload.date.getDate()}`,
        },
        headers: {
          Authorization: `Bearer ${payload.token}`,
        },
      });
      let times = [];
      res.data.available_timeslot.forEach((time) => {
        times.push({time});
      });
      times.forEach((waktu, index) => {
        waktu.status = false;
        waktu.index = res.data.id_timeslot[index]
      })
      console.log('ini time slot action', times);
      dispatch(timeSlotSuccess(times));
      return times
    } catch (error) {
      console.log('ini error', error.response.data);
      dispatch(timeSlotFailure(error));
    }
  };
};
