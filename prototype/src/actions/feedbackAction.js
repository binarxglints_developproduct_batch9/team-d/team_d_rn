import * as types from '../constant/actionType';
import axios from 'axios';
import {FEEDBACK_API} from '../constant/general';

export const getFeedbackRequest = () => ({
  type: types.FEEDBACK_REQUEST,
});

export const getFeedbackSuccess = (feedback) => ({
  type: types.FEEDBACK_SUCCESS,
  payload: feedback,
});

export const getFeedbackFailure = (error) => ({
  type: types.FEEDBACK_FAILURE,
  error,
});

export const getFeedbackAction = (feedback) => {
  return async (dispatch) => {
    try {
      dispatch(getFeedbackRequest());
      const res = await axios.get(`${FEEDBACK_API}/feedback/${feedback.id}`, {
        headers: {Authorization: `Bearer ${feedback.token}`},
      });
      dispatch(getFeedbackSuccess(res.data.data));
    } catch (error) {
      console.log('Get Feedback Error', error.response.data);
      dispatch(getFeedbackFailure(error));
    }
  };
};
