import * as types from '../constant/actionType';
import axios from 'axios';
import {BASE_URL} from '../constant/general';
import AsyncStorage from '@react-native-async-storage/async-storage';

//For logout
export const logoutSuccess = (payload) => {
  return {
    type: types.LOGOUT_SUCCESS,
    payload,
  };
};

export const logoutAction = () => {
  return async (dispatch) => {
    dispatch(logoutSuccess());
  };
};

//Handle Action Login
export const loginRequest = () => ({
  type: types.LOGIN_REQUEST,
});

export const loginSuccess = (payload) => ({
  type: types.LOGIN_SUCCESS,
  payload,
});

export const loginFailure = (error) => ({
  type: types.LOGIN_FAILURE,
  error,
});

export const loginAction = (user) => {
  let headers = {
    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
    // 'Access-Control-Allow-Origin': '*',
  };
  return async (dispatch) => {
    try {
      dispatch(loginRequest());
      const res = await axios.post(`${BASE_URL}/login`, user, {
        headers,
      });
      console.log('res', res.data.token);
      await AsyncStorage.setItem('tok', res.data.token);
      await AsyncStorage.setItem('role', res.data.role);
      dispatch(loginSuccess({token : res.data.token, role : res.data.role}));
      return res.data.token
    } catch (error) {
      console.log('ini error', error);
      dispatch(loginFailure(error));
    }
  };
};

//Handle Action REGISTER
export const registerRequest = () => ({
  type: types.REGISTER_REQUEST,
});

export const registerSuccess = (payload) => ({
  type: types.REGISTER_SUCCESS,
  payload,
});

export const registerFailure = (error) => ({
  type: types.REGISTER_FAILURE,
  error,
});

export const registerAction = (user) => {
  let headers = {
    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
    // 'Access-Control-Allow-Origin': '*',
  };
  return async (dispatch) => {
    try {
      dispatch(registerRequest());
      const res = await axios.post(`${BASE_URL}/signup`, user, {
        headers,
      });
      // console.log('res', res.data.token);
      dispatch(registerSuccess(res.data.token));
    } catch (error) {
      console.log('ini error', error);
      dispatch(registerFailure(error));
    }
  };
};

//For login
export const checkLogin = () => {
  return async (dispatch) => {
    try {
      const value = await AsyncStorage.getItem('tok');
      const value2 = await AsyncStorage.getItem('role');
      if (value !== null && value2 !== null) {
        dispatch(loginSuccess({token: value, role: value2}));
      }
    } catch (e) {
      console.log('Kosong');
    }
  };
};
