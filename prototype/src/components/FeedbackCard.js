import React from 'react';
import {Text, View, StyleSheet, Image} from 'react-native';

const FeedbackCard = (props) => (
  <View style={style.feedbackCard}>
    <Text style={style.userText}>{props.username}</Text>
    <View style={style.starList}>
      <Image
        source={{
          uri:
            'https://www.freepnglogos.com/uploads/star-png/star-vector-png-transparent-image-pngpix-21.png',
        }}
        style={style.starRating}
      />
      <Text style={style.starText}>{props.rating} / 5</Text>
    </View>
    <Text style={style.feedbackText} ellipsizeMode={'tail'} numberOfLines={2}>
      {props.review}
    </Text>
  </View>
);

const style = StyleSheet.create({
  feedbackCard: {
    justifyContent: 'center',
    width: 225,
    marginRight: 10,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: '#545454',
    padding: 10,
  },
  userText: {
    color: '#28df99',
    fontWeight: 'bold',
  },
  starList: {
    flexDirection: 'row',
    marginVertical: 2,
  },
  starRating: {
    marginRight: 5,
    width: 20,
    height: 20,
  },
  starText: {
    marginLeft: 5,
    color: 'white',
  },
  feedbackText: {
    color: 'white',
    textAlign: 'justify',
    marginBottom: 5,
  },
});

export default FeedbackCard;
