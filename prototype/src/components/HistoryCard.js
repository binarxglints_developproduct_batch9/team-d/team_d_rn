import React, {useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Modal,
  TextInput,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Stars from 'react-native-stars';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { createFeedbackAction } from '../actions/createFeedbackAction';
import { useDispatch, useSelector } from 'react-redux';
import QRCode from 'react-native-qrcode-svg';

const HistoryCard = (props) => {
  const auth = useSelector((state) => state.auth);
  const dispatch = useDispatch();
  const [bookingVisible, setBookingVisible] = useState(false);
  const [feedbackVisible, setFeedbackVisible] = useState(false);
  const navigation = useNavigation();
  const [rating, setRating] = useState(null);
  const [review, setReview] = useState(null);
  const feedback = () => {
    dispatch(
      createFeedbackAction({
        id: props.id,
        review,
        rating,
        token: auth.payload,
      }),
    );
  };

  let date = props.date.slice(0, 10);
  return (
    <View style={style.bookContainer}>
      <View style={style.textContainer}>
        <Text style={style.tag}>{props.name}</Text>
        <Text style={style.dateTag}>{date}</Text>
      </View>
      {props.transaction == false ? (
        <TouchableOpacity
          style={style.button}
          onPress={() => {
            setBookingVisible(true);
          }}>
          <Text style={style.playerText}>Booking ID</Text>
        </TouchableOpacity>
      ) : (
        <TouchableOpacity
          style={style.button}
          onPress={() => {
            setFeedbackVisible(true);
          }}>
          <Text style={style.playerText}>Feedback</Text>
        </TouchableOpacity>
      )}
      {/* Modal Booking ID */}
      <Modal
        animationType="fade"
        transparent={true}
        visible={bookingVisible}
        onRequestClose={() => {
          setBookingVisible(false);
        }}>
        <View style={style.container}>
          <View style={style.subContainer}>
            <Text style={style.checkText}>Booking E-Ticket</Text>
            <Text style={style.bookText1}>Your Booking ID :</Text>
            <QRCode size={200} value={props.id} />
            <Text style={style.bookText2}>{props.id}</Text>
            <Text style={style.bookText3}>
              Please show this Booking ID at the Field Registration section
            </Text>
          </View>
        </View>
      </Modal>
      {/* Modal Feedback */}
      <Modal
        animationType="fade"
        transparent={true}
        visible={feedbackVisible}
        onRequestClose={() => {
          setFeedbackVisible(false);
        }}>
        <View style={style.modalContainer}>
          <View style={style.modalSubContainer}>
            <Text style={style.modalText}>{'Feedback'}</Text>
            <Stars
              spacing={5}
              count={5}
              starSize={100}
              update={(val) => setRating(val)}
              fullStar={
                <Icon size={35} name={'star'} style={[style.myStarStyle]} />
              }
              emptyStar={
                <Icon
                  size={35}
                  name={'star-outline'}
                  style={style.myEmptyStarStyle}
                />
              }
            />
            {/* <Text style={style.book1Text}>Write your review here</Text> */}
            <TextInput
              style={style.book2Text}
              multiline={true}
              placeholder={'Write your review here'}
              placeholderTextColor={'#545454'}
              onChangeText={(review) => setReview(review)}></TextInput>
            <Text style={style.book3Text}>
              Please use proper language and be polite
            </Text>
            <TouchableOpacity
              onPress={() => {
                setFeedbackVisible(false);
                feedback();
              }}
              style={style.feedbackButton}>
              <Text style={style.feedbackButtonText}>Submit Feedback</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
      {props.transaction == false ? (
        <Text style={style.infoText}>Coming Up Match</Text>
      ) : (
        <Text style={style.infoText}>Done</Text>
      )}
    </View>
  );
};

const style = StyleSheet.create({
  bookContainer: {
    borderRadius: 7,
    marginBottom: 15,
    alignItems: 'center',
    paddingVertical: 10,
    width: '100%',
    borderWidth: 1,
    borderColor: '#28df99',
  },
  textContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 5,
    width: '90%',
  },
  tag: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  dateTag: {
    color: 'white',
    fontSize: 16,
  },
  button: {
    width: '90%',
    borderRadius: 7,
    borderWidth: 1,
    borderColor: '#545454',
    alignItems: 'center',
    padding: 10,
    marginBottom: 5,
  },
  playerText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  infoText: {
    color: '#FFCB74',
    fontSize: 16,
  },
  //Modal Booking ID
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  subContainer: {
    backgroundColor: '#313131',
    borderWidth: 1,
    width: '90%',
    borderColor: '#28df99',
    borderRadius: 10,
    alignItems: 'center',
    padding: 20,
  },
  checkText: {
    color: '#FFCB74',
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 20,
  },
  bookText1: {
    color: '#28df99',
    marginTop: 50,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  bookText2: {
    color: 'white',
    marginBottom: 50,
    marginTop: 10,
    fontWeight: 'bold',
    fontSize: 16,
  },
  bookText3: {
    color: 'white',
    textAlign: 'center',
  },
  //Modal Feedback
  modalContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalSubContainer: {
    width: '90%',
    paddingVertical: 15,
    alignItems: 'center',
    borderRadius: 10,
    borderColor: '#28df99',
    borderWidth: 1,
    backgroundColor: '#313131',
  },
  modalText: {
    color: '#FFCB74',
    fontSize: 16,
    fontWeight: 'bold',
    marginVertical: 15,
  },
  book1Text: {
    color: 'white',
    marginTop: 15,
  },
  book2Text: {
    borderWidth: 1,
    borderColor: '#545454',
    width: '90%',
    height: 200,
    marginVertical: 10,
    borderRadius: 10,
    textAlignVertical: 'top',
    color: 'white',
    padding: 15,
  },
  book3Text: {
    color: 'white',
    textAlign: 'center',
  },
  feedbackButton: {
    padding: 10,
    borderRadius: 7,
    borderColor: '#28df99',
    borderWidth: 1,
    margin: 15,
  },
  feedbackButtonText: {
    color: 'white',
    fontWeight: 'bold',
  },
  myStarStyle: {
    color: 'yellow',
    backgroundColor: 'transparent',
    textShadowColor: 'black',
    textShadowOffset: {width: 1, height: 1},
    textShadowRadius: 2,
    marginVertical: 10,
  },
  myEmptyStarStyle: {
    color: 'white',
    marginVertical: 10,
  },
});

export default HistoryCard;
