import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import {useDispatch} from 'react-redux';
import {
  getFieldAction,
  getFieldActionNameAsc,
  getFieldActionNameDesc,
  getFieldActionPriceAsc,
  getFieldActionPriceDesc,
} from '../actions/fieldAction';

const FilterModal = (props) => {
  const dispatch = useDispatch();

  return (
    <View style={style.filterSubContainer}>
      <TouchableOpacity
        onPress={() => {
          props.setFilterVisible(false);
          dispatch(getFieldAction());
        }}>
        <Text style={style.filterText}>Relevance</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          props.setFilterVisible(false);
          dispatch(getFieldActionNameAsc());
        }}>
        <Text style={style.filterText}>Sort by Name (A-Z)</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          props.setFilterVisible(false);
          dispatch(getFieldActionNameDesc());
        }}>
        <Text style={style.filterText}>Sort by Name (Z-A)</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          props.setFilterVisible(false);
          dispatch(getFieldActionPriceAsc());
        }}>
        <Text style={style.filterText}>Sort by Price (Low to High)</Text>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          props.setFilterVisible(false);
          dispatch(getFieldActionPriceDesc());
        }}>
        <Text style={style.filterText}>Sort by Price (High to Low)</Text>
      </TouchableOpacity>
    </View>
  );
};

const style = {
  filterText: {
    color: 'white',
    marginVertical: 15,
  },
  filterSubContainer: {
    width: '80%',
    paddingHorizontal: 15,
    backgroundColor: '#313131',
    borderColor: '#28df99',
    borderWidth: 1,
    opacity: 0.93,
  },
};

export default FilterModal;
