import React from 'react';
import { Text, View, StyleSheet } from 'react-native';

const HeaderHistory = () => (
    <View style={styles.container}>
        <Text style={styles.text}>Profile</Text>
    </View>
);

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 10,
        marginBottom: 5,
        elevation: 5,
        backgroundColor: '#313131',
    },
    text: {
        color: '#FFFFFF',
        fontSize: 24,
        fontWeight: 'bold',
    }
})

export default HeaderHistory;
