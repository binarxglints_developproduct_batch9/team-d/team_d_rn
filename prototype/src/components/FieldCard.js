import React from 'react';
import {Text, View, Image, TouchableOpacity} from 'react-native';
import {FIELD_API} from '../constant/general';
import {useNavigation} from '@react-navigation/native';

const FieldCard = (props) => {
  const navigation = useNavigation();
  return (
    <TouchableOpacity onPress={() => props.detailField()}>
      <View style={style.list}>
        <Image
          source={{uri: `${FIELD_API}/${props.image[0]}`}}
          style={style.imageList}
        />
        <View style={style.subList}>
          <View style={style.title}>
            <View style={style.fieldNameContainer}>
              <Text style={style.titleText}>{props.name}</Text>
            </View>
            <View style={style.priceContainer}>
              <Text style={style.titleText1}>
                {props.price.$numberDecimal} K
              </Text>
            </View>
          </View>
          <Text style={style.cityText}>{props.location}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const style = {
  //Field Styling
  list: {
    borderBottomWidth: 1,
    borderColor: 'white',
    alignItems: 'center',
  },
  imageList: {
    height: null,
    width: '100%',
    aspectRatio: 1.78,
  },
  subList: {
    alignItems: 'center',
    width: '90%',
    paddingBottom: 10,
  },
  title: {
    flexDirection: 'row',
    marginVertical: 5,
  },
  titleText: {
    color: '#28df99',
    fontWeight: 'bold',
    fontSize: 18,
  },
  titleText1: {
    color: '#FFCB74',
    fontWeight: 'bold',
    fontSize: 18,
  },
  fieldNameContainer: {
    width: '75%',
  },
  priceContainer: {
    width: '25%',
    alignItems: 'flex-end',
  },
  //City Styling
  cityText: {
    color: 'white',
    marginBottom: 3,
    width: '100%',
  },
};

export default FieldCard;