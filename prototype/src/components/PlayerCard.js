import React, {useState} from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Modal,
  Image,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

const HistoryCard = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const navigation = useNavigation();
  return (
    <View style={style.container}>
      <View style={style.textContainer}>
        <View style={style.nameText}>
          <Text style={style.name}>
            Denisa Ratu Sejagad Mitra Senda Kemakmuran Sejati
          </Text>
        </View>
        <View style={style.roleText}>
          <Text style={style.role}>Gk</Text>
        </View>
      </View>
      <TouchableOpacity
        style={style.button}
        onPress={() => {
          setModalVisible(true);
        }}>
        <Text style={style.text}>See Profile</Text>
      </TouchableOpacity>

      {/* Modal For Player List */}
      <Modal
        animationType="fade"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <View style={style.modalContainer}>
          <View style={style.popupContainer}>
            <Image
              style={style.image}
              source={{
                uri:
                  'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxISEA8QDxAPDw8QEA8QDxAPDxAPEA8PFREWFhUVFRUYHSggGBolHRUVITEhJikrLi4uFx8zODMsNygtLysBCgoKDg0OGxAQGy0lICUtLTMtNTU1LSstMi8rLS0tKy0xLS0tLS0tLS0tLS0tNS0tLSstLS0tLS0rLS0tLS0tLf/AABEIALEBHAMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAABAgADBAUGBwj/xABEEAABBAAEAwUDCQYFAgcAAAABAAIDEQQSITEFQVEGEyJhgQcycRQjUnKRobHB8DNCYoKy0TRzkuHxU6IVJHSTs8LD/8QAGQEBAAMBAQAAAAAAAAAAAAAAAAIDBAEF/8QAJhEBAAICAgEEAQUBAAAAAAAAAAECAxESITEEIkFRcRMUYYHRMv/aAAwDAQACEQMRAD8A8qUTUhSAhQoqEIJSBTFBoQEBSkyhCABNSjQjSCAKJgEaQQBGkVZDC57msY0ue4hrWtFkk8ggrAWTg8BLM4NhjdI46AChfqVtsdwaPBhpxxLpHWBFFIA1rxXhe4a7EWRQvQF2pBf2xeY2wYZvyWMCmNiGW5LoSSnUyEcgb1OlAUubSiov7GYlrg18mGa51UwSue82L0a1pvcbdQlg7LkPrEYnDwjNQaZWse7/AF1l66qs410bnZfnpnNiIlc45hTmk5n3YJ1qiL58iNjw9kGDbL8ofldiGuJZC0TT969jmNc0Fpc2jIauxbh4iVGbJ8YYGN7KTsZ30b8PNBRc2Rk8YDm60RmOoIa4g86K1LMHKXOaIpC5lZ6aSGXsXHYA9TpqF2eM7URRYaOI4bDygOcDJiJYC63HMQ8xfvAHWtSMt3QWNP22w8hzz4CN8/zj2yMcWMD3OO7WuIrK7eiQc3VNy5xhyJGpFg0SLF0a+KNLp+2PE8NizDLh4o4ZS1olcGiPvHZNS6tCPCfEdqHJc5PC+N2WRrmOIDgHCszTs4HmPMaKUShMaV0oQmRpdcVEIUrS1LSBCEparaQLUFRCUhWEKZUFRCUhXOakIQJSUqwBKQgrIQpWEIUgBCgCakaQKomUpApCICJCIQQBFGkaQQBMAoAmCAUiQijSAALqOwPB2z4gySPfHHhy15LRQMgIdGA+9KLQ4jmBX7wXM1sOq6HiGI+TRMhjdTg2jkJaHuNOzO6nbfbQaVSjaekqxuW17XwQ0xsjjiJw2RpzHwh7gHQua8XdNGUg75iat2nGkAd1GKsWXvog8vC0fu3YHLTe9SuqwnB8VOwZ8I4PdkNvBa4tvTNm8q3HomxPYc+I94WmtdDRIVH6lY8tUYrT4cTFivE0tJbrQN240CS7ahp6beZWPNmkcZLNP8zZ126rt8J2AcbJkroWm78qWwwXs9YBZkI63R+JT9xjj5c/b5J+HmzMMCdNXEbVVadV0XDOCOcHmN7XgNyjxZTpqXFt3lFn4+V69o3sDDQt7rBvQabJh2S7qzC8OdWjZGCvQ79FCfVU+06+kvHmGnw/DYdWyzSPL6a1gblZepJdrZrXQHmB5nN4hwl0mEGFY+N4ie04TO65Ima525+eYkgAgAb6ALUY2CRkhErC0myWusNcNyWO5ctlRBxQMdqDubaXENN6a72Pv0U62me4QvSI6lqeIYF+HkMMpZ3gDS4McH5bFgEjnVH1CrC3HaNjXtZiGa24seaNmgC34c/t5rUNWis7hmtGpQhAtT0pS6iqIQpWkJSEFRCFKylEFRCQhWkJSEFVIOCsIQcEFRCFKwtS0gVEhRQoAiEVEEpEBEBEBBKRCNKUgIRRaE1IAAmARARAQZnAoS6dhAByB0hsW3wgkX60vQOy0cUMWIx+IyvcZC0OcPdaxuc0D7tucfu6rl+y+G+YxkprUMhZrVH33Xz5NGnVa3j3aKsEzCxEAPfnc7Nb3DUkHprl6e7Srv3Ol2PqNu4w3bUzuIiFNJN8ufNbuHxDxc9h+a8m7FYgMd+tl6hhJS+i02NNtlhzxqXpYJ3XbOa0AkDYDTyKzWwjKAsHxAX8bKyoJCTQWaP5XzHTa4XBitVXiMHroronEfCkTLZV9qUmNaZuV4tvbQ9o8I3uHWAeh3oryPiMBDiNtSOW40/XxC9v4vh+8ic3qOW5+HmvLMZgXal2pBokDoCLo7XpY5KzD7Z0hm3arWxsvDyMB8RZZYWjLbCCCCdzX61WoY1dFHFkc03oSGmiQQCNqHnRXPgUSOi20lgyQNIEJwFHBTVq6QpOhSBCEpCtpKQgrISlqsIQIQVFqUhWEIEIKnBBO4JKQVqKUmpAqKiICAgJqRARQABFEI0gLQmpQBMAglJgFAmAQb3gGIPybExD3i5pZqRbnMcK/wC2v5gh2z7FR4WPDvmxLWNc0tNMzPeW+8WtG+poWq+zEwEpYdnmJ19O7kD69QCt77UW99xXDMd+ygw+vNoolzz5CsqhbqJlbTuYhyOA4lgIRlAxB1uy1lmvLNsvQeyXGsPPlbG8h22V7cpsfcVzj+0kbo3M+SQyxsbmyy6ylgFk0AcmmvPloue4xiBh2YfF4NvcDEFwLO8ZK0OayN9W2takFgiwbBohZ5pz8tUZJp89PcsXFUZdWwtYUfE4omZ5HUKsgAkrm/Z72ufjQYZW+JuUudycPh9i7jiHDYRGSIw4tboKuzuBXNZ5pq34aIye2I+3O4vt7EKZDhcTK93utDTZHUAArZYM42aMSCCPDn6Er8xPqCKXGcR4hi2vlMLmRFjWv7sZXZhY9410N6Hktt2N7WYqbDukmib83lEhALTZc8ZQ01nNNa407QPGhOhtjVo3KFt0nUOqw2IlFMxMXduJoOb4o3fAi8p8itZ2g4UMr8QweIDxg6tLdifI0Sujw0/exhxbuNWnWlj8VgzYedouzFJXxylQmv0cvuHlGPFOtv0mk6aWTY320/ArlpXeN9/Sd+K6qDBS4s5cOAXtyZnH9n8T5UKqlR2u7HnBtMrcQMRTm/KGmIxGMyE5XNsnM2/DfIkddNWO8eJ8suWlpjcR059qJCSNXAK9lVUjlTUiUFRCUhWpSgrISuTkJSEFeVAhW0lIQVEJKVrgkpBQESoEUCpwEKThBFEQogICcBABOAgICNIgJqQBNSlJgEG64TiIooAXQRyTTTiMSyPcHMZcbT3Y0aD84d9yddNFV7SsVM3islOIa+KIsJuqfCzMB6hariTAcHMSDcZbRsjIXyx0R5014XUe1t7PlOBsDP8AJ2d4RRA0H+/2KvvvbTPHjTj9d/nf+aY/YvhJBLpO6eHXZEYEmo1twF36rR+0uVvfwwsaI44WuysHIuNk+tD7F0+Fx7cLA5wOmU7m9f0V5lxPHOnmdI7Uk6eQVGPdrbX5orSmnpXsUg8eIk5DI0D7SfyXrcrr0G6899kuGyYLMRRlkeQR+8NvyXaGN7T7py+e6qy27ldjpusfhgTcKZnzlgzXdgA/itrhJGNaGhrR6aedqjG0CDqA7r16IwsuvyVUWms6hbaOVdy2In22Qa6752NvJVyRUNUMMzxb6KfKeUQo4xrbl+xuGGGZimu8JOLfE00SS1vhZ6UCfVT2isHybEFxaWuw7yCDoSJYHM/79Fs8ThXPhma1uZ8mJk7uhsQ/Rx6DTfzXH+0iV8OBwuGkLe9nle54Ds3zUZDiP9ZZ/pTHu2TX9rMnCmKbb77jX5jy4KFZQCx4AssBek8dXlUyp6UKCohKWqwhAhBUWpCFaQkIQJSWlbSrKBCEhCsISlBhp2pQE4QSkQooEBTAJVY0IC0JgEGp0BCdKAmCAgIhQIgINlwGCOV8mHmGZk8YAbr4pI5GStbp9LIW/wAy1najFyYjD8MxMg8RglheRY8cMzmgnnZFKxlgggkEEEEaEEbELM7TPvh2GdzOKxIdTRVuDHuPkSSCuSnWfhncG4MMdA1pdlveuS5/jvZpuCmYXSCaMHxMHgkI8uV7LP7G9ofk8cw3c1pcAdsv/JC0seIlxWKzyW+SQ2GakBu9eQWSsWiZ+m+9q2iJ127L2ecUxRL4mZBhmP7yMSW2RmurdLBv0pemFnf1NHicRFlFFjTCWF13bgWk+W64Hs/KzD5swaXOHiawhzh6c61Houp4VxWFl2XHOdqrX71XM7t46aa4rcNx5b+eNr4yxzhnrRxFgHkaWJhi6N+R2rSfCfy+Kqf2hw4rNFiAHOLQ8YeVwBHMkN0+JTTYtr/CwOF6scWObry3GmyhkiI7crW8dTHTYyz3onz0AB1+1YgFnVZLG/8AChS0zbaFqxEaavivabCYFrvlE4Etd42BnimcHE5crOl8zQ81452i4/Lj8S7ESgMFBkUTTmbFGNavmSSSTzJ6UBs/al4uKSDmyDDMP/t5v/uudhjXo4qRWHm5bzaWVAFkhVQtWQrVSohSkxSoAgiUECkJCEziq0Ecq6TlCkFZCFJkCgw6UCKiAtRAQCZqCUrAgAmQEBEKBEBAwTAJQE6ApmoBQBBa1W8WIdw4s5x4oPvoHx1/+f3qgFXhueOSM7SNoeTgbaft/FclKvlpuy2KjixUbpwDA4mOcH/pu5+ho+i6Htzw8wzXgQ6Nrm5XuYRbg4ita09Oq5QxFpIcKIsOB3C7PspxVrm91OQ4NaG24XTc1N187Wa/U8obcXjjMquwuGLXuMlOkcBeZtuNdXHVencFfGHA00nXTJdEfGqWnwroAAGNBdtmGuU9b9Qum4RC0NHh1pU8rTZt/U44+LMoXmq/LdVzMF5jqfx5Kx961oOvRV5SdBsd+YtRtM/KiJLEdf7rYYTD21xOywsPESa2FrbGdoYWt1Ox8iu4a/MoZrT4h4Z7R4z/AOK4rTcYct+r8njH5FaGJq7T2o4asex/KTDRn1a97T91Llwxb6+Hn3/6kIwrCoAopIkpCkyVACkKsKQBBWQlcriEpCCqkrgriFW5BUQlVjlXSDERCAT0ggCICIRQEIhBOEBCakoToGCijUyAtT0lAThAKTsNII0gvxWFilglkkOR8Xd+MCxkcS05xzrw6+fNaLEcOe0OcCPBV5Tdg8x/ddBhY88eJi/6mGmr6zB3g/oXI4LiD2UAdBsDysUfTy2Vdo76X0t129M7GyBze8DhZ1LeWYbk9On607nDYnWhoK0+3mvD+F8QyvuMFt+8Gk1fkPyXYYHHSkAgvdZrRp1rlSyXrMS20tuHpzsTdevMaaKmTiAbbWM7x1daFVzI23Wh4W2WSgQ8dbGX89Vv48JQ2ra9lRa1vhZxrHljQvmkdT35W82xjKK6E7rfRNAFCqG1ClrGCq5cyPNbKE2Ew7+Ucv8ADz/2tQ07Ay8i2eMnzBY5v4uXEgL072n4TPgBIN4J2PP1XAsP3uavMGFeljn2vNyx7jUlKchClYrIlJTlVlBEqsDVMqCoqFO5qQoEcUjkxQKCohDKrHJCUGCEygUCBgmQamQQBM1QBEBAQmQCakBanpI1OEDBqYBBqYIGCYJQigzuEmpohyc4MPwf4T9xK4KeAse5h3Y4tPoaXoPBcEZMVhoT4DJMxvi0IGria32aVyvbHC93j8S0Ch3ryB8So2WV8MTgslYiG/d7xgPwtfQuBwrC1pDRty8184R/eNR5FfQPYHioxWEY+x3jPBIOjh/fdZs1e4lrwz7ZdLh4WjYBXPbp5KkafBO52hVfWnZidsEt1Cz8OFgHdbCAqrH5WZPBOIYFs8E0D/dljcwnoSND6Gj6LwvunRvfFK3JJG4se08nDde+EXS859qfEIGSxRNa12ILfn3ADMI9MgP8W5B3ryK24p70x5Y624wpCrXMIFjxCrJGtDz6JKtXs6tRrU5amAQIlJTuSOQVuKQhOUEFRahlVpSlBUWoZVYUlINcCigEyAtKZKAnAQMEUAiEBThLSZqAhPaVGMEmmguPINBJ+wIHamzLP4bwSWR4a4GJlFznOGoaByHXl6rcw8OiY9mSNrgDTnvOct/io6ZumiDn4sM4tzmmRWB3snhZZ2r6R8ha3/DMNDFle0ukky2XPAaGh2gyt5aWb3ScSk+USd24fNM1y9AOd9f7rHkxXiFfvOJ6e6NPxQcfjuNSw4x8wNStlbiIidid6+ANj0KnarijMTiPlMeglGYtPvMderXfA8+dLH7ZQfOZvM8uRN/ib/nWhw8laclyYSrOnS8PwAmBA0cB9q63sIJsNMcriGuFObrRpcZwDFZJmXsSAV6zBg/de0bgfAHqsWe016ej6esWjbr8NjS4a/FZJlsLRYN5G+gWbhMRmJHRZIyT4Xzj+WXqCthhysQx87AAFkmgAOdlcH2t9pbGNdBw0iSQ6OxdB0bL37q/fd/F7o5ZuWjBjtMs+a8RDoe3HbaPAtMUWWXGuHhYdWQA/vy18dG7nyGq8ZM0ksjpZXOkkkdmc52rnOPVUakl8ji5ziXOLnFznOO5cTqSfNZ/D4czhenQdB5r0K1isdPPtabOh4OA1h56W53nrt5b/eeapOHDv2dZgac2wBfIi/iro5APAK2vzKxhIGSNfqc4LHNFAEg6FdRUyNc12VwLT0KFraOc0vlLwHDM0G+lbg8liPwZzEM1A+lv8PNBikqsp5GkGnAg9CqigjglKjilJQRyQpyktApS0mQtBrgmQCiBgnCRqcICmCVMgdZnD+FTTn5pnh5vd4WD15+lqcG4f38mUnLEwZ5n/RZ0Hmdh/suhi4r82MpBitzWPaByF0QNGuA1rYgEiqIQLhOzEbf20neu+izwsH5lbDBYmARuZhG5HNeY5XFuWRrxuCOXXzBC1xxbuuVw1BHMb6dVj4HE/wDnZ9h3sUUjh1kZmaXfZl+xBvpZ8tNB3vMTu4Vev3LHz5Y75nxH12WHPL4m689Viz4iy8abho15BtoGbJTXu5vuuvwWoxWJqaNt6BtXyJ3KzXvsOH0WEetX+a5HE4ggxk/SQbXj0LXCzrpX68lyEuGI1brS6PGyEm+RC1TmUSgx8ObpzfeBFt6+bfPyX0H2bi77CRPHNjXA78l4D3V6jQ9F3HYj2gSYEd1Owz4XoCBLF5sJ0I/hP2jnTmxc4aMObg9DnkcwHMAK9fsUwfFoYInYnFStijFjXVzjrTWtGrneQXD9tfaCyc5MA12UjxSSMLT8ACuEklc92aV7pHDYucTXkByWXH6Sd7s1ZPWRx1V1va7ttNjyY2B2HwQ2hsZ5vOYj+kafFc22UN21PJY+YnQfaslrA0fxHqt8RERqHnWtNp3KzD2Xa6nfyb/v5roMG0NC0GDFa8zutgzEFdcbUS1KB1B8uSjmgwxkuGckvaPLN/ZYEUpMjQTs0qvhkhklaT7rAGt9Nz+KDeZ7dMB1aCru7skHfRUcKOjnHeR73fyg0FlidrQ+QkAD7ygp4q4fNMGrzoOtrWzwEffp0rdZvDwZJPlMgpuojvpdEq0tzZydPES3kR5oNLSCy8Tha8Qoa0W7X5tH4j7NNsVAjlAESFCgQqsp3FKgwAigogZqcIqIIE6iiDoOz/8AhMb9eD+mRYXZ/wDw2K/9fhP/AJlFEGU39nh/g78Fj4T/ABjP8h/9bVFEGxxHvj4/msN3P6zv6AoogSLZ/wBX8iuRx/uj6yiiDJl90LDfv+vNRRBWzf8AmP5IY3dnxUUQO3kiFFEF8Ozfh+atxPL0UUQZGHWSxRRBZgv2zvq/kEvZ/wB0fE/gUVEG3wOzf8oJeKf4dv1j+JUUQbOL9lF/lD8kW7frqoogx37O+B/pWrm94/FRRAiVyiiCpyVRRB//2Q==',
              }}
            />
            <Text
              style={style.profileNameText}
              ellipsizeMode={'tail'}
              numberOfLines={2}>
              Denisa Ratu Sejagad Mitra Senda Kemakmuran Sejati
            </Text>
            <View>
              <Text style={style.bioText}>Bio</Text>
              <Text style={style.bioDetailsText}>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
                sodales tempus quam sed sagittis. Pellentesque justo mi,
                suscipit quis viverra vitae, porttitor vitae est. Cras auctor ut
                augue a posuere.
              </Text>
            </View>
            <TouchableOpacity style={style.closeButton}>
              <Text
                onPress={() => {
                  setModalVisible(!modalVisible);
                }}
                style={style.closeText}>
                Close
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  );
};

const style = StyleSheet.create({
  container: {
    borderRadius: 7,
    borderWidth: 1,
    borderColor: '#28df99',
    marginBottom: 15,
    alignItems: 'center',
    paddingVertical: 10,
  },
  textContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '90%',
    marginBottom: 10,
  },
  nameText: {
    width: '85%',
  },
  name: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  roleText: {
    width: '15%',
    alignItems: 'flex-end',
  },
  role: {
    color: '#FFCB74',
    fontWeight: 'bold',
    fontSize: 18,
  },
  button: {
    width: '50%',
    borderRadius: 7,
    borderColor: '#545454',
    borderWidth: 1,
    alignItems: 'center',
    padding: 5,
  },
  text: {
    color: '#FFFFFF',
    fontSize: 16,
  },
  //Modal
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  popupContainer: {
    padding: 25,
    opacity: 1,
    backgroundColor: '#313131',
    width: '80%',
    borderWidth: 1,
    borderColor: '#28df99',
    alignItems: 'center',
    borderRadius: 10,
  },
  image: {
    width: '35%',
    height: null,
    aspectRatio: 1,
    borderRadius: 50,
    borderWidth: 1,
    borderColor: '#28df99',
  },
  profileNameText: {
    color: '#28df99',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  bioText: {
    color: '#28df99',
    fontWeight: 'bold',
    marginBottom: 2,
  },
  bioDetailsText: {
    color: 'white',
    textAlign: 'justify',
    marginBottom: 30,
  },
  closeButton: {
    borderRadius: 7,
    padding: 5,
    width: '50%',
    borderWidth: 1,
    borderColor: 'red',
  },
  closeText: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
export default HistoryCard;
