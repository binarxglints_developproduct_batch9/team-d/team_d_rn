export const BASE_URL = 'https://soka.kuyrek.com:3005';

export const FIELD_API = 'https://soka.kuyrek.com:3001';

export const TIMESLOT_API = 'https://soka.kuyrek.com:3003';

export const FEEDBACK_API = 'https://soka.kuyrek.com:3002';
