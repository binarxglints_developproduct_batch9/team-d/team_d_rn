import React, { useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Provider, useSelector, useDispatch } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { checkLogin } from './src/actions/authAction';

import landingPage from './src/screen/landingPage';
import loginPage from './src/screen/loginPage';
import signUpPage from './src/screen/signUpPage';
import homePage from './src/screen/homePage';
import homeDetails from './src/screen/homeDetails';
import historyPage from './src/screen/historyPage';
import profilePage from './src/screen/profilePage';
import playerList from './src/screen/playerList';
import searchField from './src/screen/searchField';
import bookingPage from './src/screen/bookingPage';
import editProfile from './src/screen/editProfile';
import managerPage from './src/screen/managerPage';
import confirmationPage from './src/screen/confirmationPage';
import store from './src/store';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const RootHome = () => {
  return (
    <Tab.Navigator
      initialRouteName="homePage"
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color }) => {
          let iconName;

          if (route.name === 'Home') {
            iconName = 'home';
          } else if (route.name === 'History') {
            iconName = 'event-note';
          } else if (route.name === 'Profile') {
            iconName = 'account-circle';
          }

          // You can return any component that you like here!
          return <Icon name={iconName} size={32} color={color} />;
        },
      })}
      tabBarOptions={{
        showLabel: false,
        activeTintColor: '#28df99',
        inactiveTintColor: 'gray',
        style: {
          backgroundColor: '#313131',
          borderTopWidth: 1,
          borderColor: 'white',
        },
      }}>
      <Tab.Screen name="Home" component={homePage} />
      <Tab.Screen name="History" component={historyPage} />
      <Tab.Screen name="Profile" component={profilePage} />
    </Tab.Navigator>
  );
};

const rootManager = () => {
  return (
    <Tab.Navigator
      initialRouteName="managerPage"
      screenOptions={({ route }) => ({
        tabBarIcon: ({ color }) => {
          let iconName;
          if (route.name === 'managerPage') {
            iconName = 'home';
          } else if (route.name === 'Profile') {
            iconName = 'account-circle';
          }
          // You can return any component that you like here!
          return <Icon name={iconName} size={32} color={color} />;
        },
      })}
      tabBarOptions={{
        showLabel: false,
        activeTintColor: '#28df99',
        inactiveTintColor: 'gray',
        style: {
          backgroundColor: '#313131',
          borderTopWidth: 1,
          borderColor: 'white',
        },
      }}>
      <Tab.Screen name="managerPage" component={managerPage} />
      <Tab.Screen name="Profile" component={profilePage} />
    </Tab.Navigator>
  );
};

const authScreen = () => {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="landingPage" component={landingPage} />
      <Stack.Screen name="loginPage" component={loginPage} />
      <Stack.Screen name="signUpPage" component={signUpPage} />
    </Stack.Navigator>
  );
};

const userScreen = () => {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="homepage" component={RootHome} />
      <Stack.Screen name="homeDetails" component={homeDetails} />
      <Stack.Screen name="playerList" component={playerList} />
      {/* <Stack.Screen name="bookPage" component={bookPage} /> */}
      <Stack.Screen name="bookingPage" component={bookingPage} />
      <Stack.Screen name="searchField" component={searchField} />
      <Stack.Screen name="editProfile" component={editProfile} />
      <Stack.Screen name="confirmationPage" component={confirmationPage} />
    </Stack.Navigator>
  );
};

const managerScreen = () => {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="managerPage" component={rootManager} />
      <Stack.Screen name="editProfile" component={editProfile} />
    </Stack.Navigator>
  );
};

const App = () => {
  const payload = useSelector((state) => state.auth);
  const userList = useSelector((state) => state.user.userList);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(checkLogin());
  }, []);

  return (
    <NavigationContainer>
      <Stack.Navigator headerMode="none">
        {payload.payload == "" && (
          <Stack.Screen name="authScreen" component={authScreen} />
        )}
        {payload.payload != "" && payload.role == 'user' && (
          <Stack.Screen name="userScreen" component={userScreen} />
        )}
        {payload.payload != "" && payload.role == 'manager' && (
          <Stack.Screen name="managerScreen" component={managerScreen} />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

// export default App;
export default () => {
  return (
    <Provider store={store}>
      <App />
    </Provider>
  );
};
